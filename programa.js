class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.61253, -74.1066];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono (posicion)
    {
        this.poligonos.push(L.polygon(posicion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.61253, -74.1066]);
miMapa.colocarMarcador([4.627946, -74.065498]);

miMapa.colocarCirculo([4.61253, -74.1066], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
});

miMapa.colocarCirculo([4.577525,-74.208944], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 300
});

miMapa.colocarPoligono([

  
    [4.644799, -74.078575],
    [4.644653, -74.077190],
    [4.645168, -74.076288],
    [4.645884, -74.075636],
    [4.647018, -74.076645],
    [4.647147, -74.077049],
    [4.647229, -74.078341],
    [4.644799,-74.078575]
   
]); 

